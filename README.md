# NeuroRacer Installer
To install the NeuroRace project on a Jetson TX2 follow the prerequisites and execute the [installNeuroRacer shell script](installNeuroRacer.sh). The optional script to build OpenCV from source is based on the official repositories from JetsonHacks but has been customized with different compiling flags and automized to not require any user input.

## On this page
- [Prerequisites](#prerequisites)
- [Installation](#installation)
  - [OpenCV from Source](#opencv-from-source)
  - [Performance Tuning](#performance-tuning)
- [Additional Hardware](#additional-hardware)

## Prerequisites 

* Flash Jetson

An NVIDIA developer account is needed.
Use the [NVIDIA SDK Manager](https://developer.nvidia.com/nvidia-sdk-manager) and choose your Jetson device (for example TX2). Afterwards select all components to be installed as seen in the following figure. The Host Components and Target Additional SDKs are not needed.

Tested Dec. 2019 with NVIDIA JetPack 4.2.3 (L4T 32.2.1).

During the installation process, Ubuntu 18.04 is installed. You need a HDMI monitor and a keyboard to complete Ubuntu setup. 

| ![packages](docs/tx2_flash_packages.png) |
|:--:|
| _NVIDIA SDK Manager ([https://developer.nvidia.com/nvidia-sdk-manager](https://developer.nvidia.com/nvidia-sdk-manager))_ |

<br>

* Remove sudo's timeout

The install script will run for some time, requiring sudo to install and build packages as well as creating directories and files. To prevent required user interaction while installing, deactivate the timeout.

```
edit /etc/sudoers
```

Add:

```
Defaults        timestamp_timeout=-1
```

* Update system and clean cache

```
sudo apt update  
sudo apt upgrade  
sudo apt autoremove  
sudo apt autoclean  
sudo apt clean
```

* Reboot system

## Installation
* Clone `neuroracer-robot-install` repository to user's home directory

```
git clone https://gitlab.com/NeuroRace/neuroracer-robot-install.git
```
* Execute [`installNeuroRacer.sh`](installNeuroRacer.sh) (installs required packages and sets up NeuroRace project)
* Execute [`buildCustomKernelTX2.sh`](buildCustomKernelTX2.sh) (required for acm, wireguard and xpad)
* Reboot system

__Note:__  
Pay attention when updating your Jetson Ubuntu Software to not override customized packages (`opencv` - if built from source, `linux-kernel` - if custom one is used).

#### Build OpenCV from Source
Since the project makes use of the ZED camera, the onboard OmniVision is not used and the provided OpenCV from JetPack can be used. But to build OpenCV from source and make use of the OmniVision Camera for example, purge all opencv packages (or don't flash OpenCV from Jetpack) and run [third_party/buildOpenCV_custom.sh](third_party/buildOpenCV_custom.sh). The script uses custom flags and requires no user interaction.
* Reference: [https://github.com/jetsonhacks/buildOpenCVTX2](https://github.com/jetsonhacks/buildOpenCVTX2)

CMake configures OpenCV. Here is a good reference configuration:
```
-- General configuration for OpenCV 3.4.2 =====================================
--   Version control:               3.4.2
-- 
--   Platform:
--     Timestamp:                   2019-11-29T13:48:56Z
--     Host:                        Linux 4.9.140-tegra aarch64
--     CMake:                       3.10.2
--     CMake generator:             Unix Makefiles
--     CMake build tool:            /usr/bin/make
--     Configuration:               Release
-- 
--   CPU/HW features:
--     Baseline:                    NEON FP16
--       required:                  NEON
--       disabled:                  VFPV3
-- 
--   C/C++:
--     Built as dynamic libs?:      YES
--     C++11:                       YES
--     C++ Compiler:                /usr/bin/c++  (ver 7.4.0)
--     C++ flags (Release):         -fsigned-char -W -Wall -Werror=return-type -Werror=non-virtual-dtor -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations -Wundef -Winit-self -Wpointer-arith -Wshadow -Wsign-promo -Wuninitialized -Winit-self -Wsuggest-override -Wno-narrowing -Wno-delete-non-virtual-dtor -Wno-comment -Wimplicit-fallthrough=3 -fdiagnostics-show-option -pthread -fomit-frame-pointer -ffunction-sections -fdata-sections    -fvisibility=hidden -fvisibility-inlines-hidden -O3 -DNDEBUG  -DNDEBUG
--     C++ flags (Debug):           -fsigned-char -W -Wall -Werror=return-type -Werror=non-virtual-dtor -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations -Wundef -Winit-self -Wpointer-arith -Wshadow -Wsign-promo -Wuninitialized -Winit-self -Wsuggest-override -Wno-narrowing -Wno-delete-non-virtual-dtor -Wno-comment -Wimplicit-fallthrough=3 -fdiagnostics-show-option -pthread -fomit-frame-pointer -ffunction-sections -fdata-sections    -fvisibility=hidden -fvisibility-inlines-hidden -g  -O0 -DDEBUG -D_DEBUG
--     C Compiler:                  /usr/bin/cc
--     C flags (Release):           -fsigned-char -W -Wall -Werror=return-type -Werror=non-virtual-dtor -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations -Wmissing-prototypes -Wstrict-prototypes -Wundef -Winit-self -Wpointer-arith -Wshadow -Wuninitialized -Winit-self -Wno-narrowing -Wno-comment -Wimplicit-fallthrough=3 -fdiagnostics-show-option -pthread -fomit-frame-pointer -ffunction-sections -fdata-sections    -fvisibility=hidden -O3 -DNDEBUG  -DNDEBUG
--     C flags (Debug):             -fsigned-char -W -Wall -Werror=return-type -Werror=non-virtual-dtor -Werror=address -Werror=sequence-point -Wformat -Werror=format-security -Wmissing-declarations -Wmissing-prototypes -Wstrict-prototypes -Wundef -Winit-self -Wpointer-arith -Wshadow -Wuninitialized -Winit-self -Wno-narrowing -Wno-comment -Wimplicit-fallthrough=3 -fdiagnostics-show-option -pthread -fomit-frame-pointer -ffunction-sections -fdata-sections    -fvisibility=hidden -g  -O0 -DDEBUG -D_DEBUG
--     Linker flags (Release):      
--     Linker flags (Debug):        
--     ccache:                      NO
--     Precompiled headers:         NO
--     Extra dependencies:          dl m pthread rt cudart nppc nppial nppicc nppicom nppidei nppif nppig nppim nppist nppisu nppitc npps cublas cufft -L/usr/local/cuda-10.0/lib64
--     3rdparty dependencies:
-- 
--   OpenCV modules:
--     To be built:                 calib3d core cudaarithm cudabgsegm cudacodec cudafeatures2d cudafilters cudaimgproc cudalegacy cudaobjdetect cudaoptflow cudastereo cudawarping cudev dnn features2d flann highgui imgcodecs imgproc java_bindings_generator ml objdetect photo python2 python_bindings_generator shape stitching superres ts video videoio videostab viz
--     Disabled:                    js python3 world
--     Disabled by dependency:      -
--     Unavailable:                 java
--     Applications:                tests perf_tests apps
--     Documentation:               NO
--     Non-free algorithms:         NO
-- 
--   GUI: 
--     QT:                          YES (ver 5.9.5)
--       QT OpenGL support:         NO
--     GTK+:                        NO
--     VTK support:                 YES (ver 6.3.0)
-- 
--   Media I/O: 
--     ZLib:                        /usr/lib/aarch64-linux-gnu/libz.so (ver 1.2.11)
--     JPEG:                        /usr/lib/aarch64-linux-gnu/libjpeg.so (ver 80)
--     WEBP:                        /usr/lib/aarch64-linux-gnu/libwebp.so (ver encoder: 0x020e)
--     PNG:                         /usr/lib/aarch64-linux-gnu/libpng.so (ver 1.6.34)
--     TIFF:                        /usr/lib/aarch64-linux-gnu/libtiff.so (ver 42 / 4.0.9)
--     JPEG 2000:                   build (ver 1.900.1)
--     OpenEXR:                     build (ver 1.7.1)
--     HDR:                         YES
--     SUNRASTER:                   YES
--     PXM:                         YES
-- 
--   Video I/O:
--     DC1394:                      NO
--     FFMPEG:                      YES
--       avcodec:                   YES (ver 57.107.100)
--       avformat:                  YES (ver 57.83.100)
--       avutil:                    YES (ver 55.78.100)
--       swscale:                   YES (ver 4.8.100)
--       avresample:                YES (ver 3.7.0)
--     GStreamer:                   
--       base:                      YES (ver 1.14.5)
--       video:                     YES (ver 1.14.5)
--       app:                       YES (ver 1.14.5)
--       riff:                      YES (ver 1.14.5)
--       pbutils:                   YES (ver 1.14.5)
--     libv4l/libv4l2:              NO
--     v4l/v4l2:                    linux/videodev2.h
--     gPhoto2:                     NO
-- 
--   Parallel framework:            pthreads
-- 
--   Trace:                         YES (built-in)
-- 
--   Other third-party libraries:
--     Lapack:                      NO
--     Eigen:                       YES (ver 3.3.4)
--     Custom HAL:                  YES (carotene (ver 0.0.1))
--     Protobuf:                    build (3.5.1)
-- 
--   NVIDIA CUDA:                   YES (ver 10.0, CUFFT CUBLAS FAST_MATH)
--     NVIDIA GPU arch:             62
--     NVIDIA PTX archs:
-- 
--   Python 2:
--     Interpreter:                 /usr/bin/python2.7 (ver 2.7.15)
--     Libraries:                   /usr/lib/aarch64-linux-gnu/libpython2.7.so (ver 2.7.15+)
--     numpy:                       /home/nvidia/.local/lib/python2.7/site-packages/numpy/core/include (ver 1.16.5)
--     packages path:               lib/python2.7/dist-packages
-- 
--   Python (for build):            /usr/bin/python2.7
-- 
--   Java:                      
--     ant:                         NO
--     JNI:                         NO
--     Java wrappers:               NO
--     Java tests:                  NO
-- 
--   Matlab:                        NO
-- 
--   Install to:                    /usr
-- -----------------------------------------------------------------
-- 
-- Configuring done
-- Generating done
-- Build files have been written to: /home/nvidia/opencv/build
```

You may need to add an option for cmake to find the CUDA SDK libraries:
```
    -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-10.0
```

#### Performance Tuning
* To remove bloat services wasting resources, i.e. GUI, execute the according script
  * [disable services](scripts/disable_services.sh)
  * reboot
* To reactivate
  * [enable services](scripts/enable_services.sh)
  * reboot

__Note:__  
The scripts should run via console/ssh due to GUI de-/activating services.

## Additional Hardware
For information how to install additional supported hardware for the robot, see [neuroracer-robot](https://gitlab.com/NeuroRace/neuroracer-robot/tree/master#add-ons-sources-not-included). 
