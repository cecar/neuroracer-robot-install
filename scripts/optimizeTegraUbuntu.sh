#!/bin/bash
# remove unnecessary packages
sudo apt -y purge docker docker.io snapd ubuntu-core-launcher squashfs-tools thunderbird libreoffice-core libreoffice-base-core libreoffice-common
sudo apt -y autoremove
