#!/bin/bash

function stop_services {
	sudo systemctl disable $1
	sudo systemctl stop $1
	sudo systemctl mask $1
}

service_files[0]="snapd"
service_files[1]="lightdm"
service_files[2]="ModemManager"
service_files[3]="colord"
service_files[4]="whoopsie"
service_files[5]="cups"

for service in ${service_files[@]}
do
	echo "disabling: $service"
	stop_services $service
done

echo ""
echo "Done"
